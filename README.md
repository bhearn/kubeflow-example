# Kubeflow Example

Instructions to deploy Kubeflow on GCP's GKE.

**IMPORTANT:**

- YOU MUST HAVE A PAID GCP ACCOUNT TO DEPLOY KUBEFLOW!!!! >:( ... see [here](https://github.com/kubeflow/website/issues/1065#issuecomment-524141958) and [here](https://www.kubeflow.org/docs/gke/deploy/project-setup/)
- yq 4.0.0 will not work. You must use yq 3.4.1 ... see [here](https://github.com/kubeflow/website/issues/2419)
- Kustomize 3.9.0 will not work. You must use Kustomize 3.8.8 ... see [here](https://github.com/kubernetes-sigs/kustomize/issues/3340)

**Prerequisites:**

- GCP account with billing enabled
- [gcloud sdk](https://cloud.google.com/sdk/docs/quickstart)
- [gcloud components, Kustomize, & yq](https://www.kubeflow.org/docs/gke/deploy/management-setup/#install-the-required-tools)
- [Anthos Service Mesh](https://cloud.google.com/service-mesh/docs/archive/1.4/docs/gke-install-new-cluster#setting_up_your_project)

## Step 1: Set up a Google Cloud Project

1. Create a project in the GCP dashboard.
2. Enable the required APIs:
```
gcloud services enable \
  compute.googleapis.com \
  container.googleapis.com \
  iam.googleapis.com \
  servicemanagement.googleapis.com \
  cloudresourcemanager.googleapis.com \
  ml.googleapis.com \
  meshconfig.googleapis.com
```
3. Set environment variable:
```
export PROJECT_ID=<YOUR_PROJECT_ID>
```
4. Initialize your project and prepare it for the Anthos Service Mesh:
```
curl --request POST \
  --header "Authorization: Bearer $(gcloud auth print-access-token)" \
  --data '' \
  https://meshconfig.googleapis.com/v1alpha1/projects/${PROJECT_ID}:initialize
```

## Step 2: Set up OAuth for Cloud IAP

1. Setting up OAuth for Cloud IAP must be done through the GCP dashboard: [https://www.kubeflow.org/docs/gke/deploy/oauth-setup/](https://www.kubeflow.org/docs/gke/deploy/oauth-setup/)

## Step 3: Set up a Management Cluster

1. Set environment variables:
```
export MGMT_PROJECT=<the project where you deploy your management cluster>   
export MGMT_DIR=<path to your management cluster configuration directory>    
export MGMT_NAME=<name of your management cluster>                           
export LOCATION=<location of your management cluster>                        
```
2. Fetch the management blueprint to current directory, change to the Kubeflow directory, fetch the upstream management package, use kpt to set values, create the management cluster, create a kubectl context for it, install the Cloud Config Connector, set the managed project, and update the policy:
```
kpt pkg get https://github.com/kubeflow/gcp-blueprints.git/management@v1.2.0 "${MGMT_DIR}"

cd "${MGMT_DIR}"

make get-pkg

kpt cfg set -R . name "${MGMT_NAME}"

kpt cfg set -R . gcloud.core.project "${MGMT_PROJECT}"

kpt cfg set -R . location "${LOCATION}"

make apply-cluster

make create-context

make apply-kcc

kpt cfg set ./instance managed-project "${MGMT_PROJECT}"

gcloud beta anthos apply ./instance/managed-project/iam.yaml
```

## Step 4: Install Anthos Service Mesh

1. Set environment variables:
```
export GCP_EMAIL_ADDRESS=<your GCP email address>
export PROJECT_NUMBER=$(gcloud projects describe ${PROJECT_ID} --format="value(projectNumber)")
export CLUSTER_NAME=${MGMT_NAME}
export CLUSTER_LOCATION=${LOCATION}
export WORKLOAD_POOL=${PROJECT_ID}.svc.id.goog
export MESH_ID="proj-${PROJECT_NUMBER}"
export CHANNEL=<your channel> # can be one of the following: regular, stable or rapid
export SERVICE_ACCOUNT_NAME=<your service account name>
export SERVICE_ACCOUNT_KEY_PATH=/tmp/creds/${SERVICE_ACCOUNT_NAME}-${PROJECT_ID}.json
export MEMBERSHIP_NAME=<your membership name>
```
2. Set the required Identity and Access Management (IAM) roles, enable the required APIs, set default region, get credentials, grant cluster admin permissions to the current user, create the service account, bind the gkehub.connect IAM role to the service account, download the service account's private key JSON file, register the cluster, and install Anthos Service Mesh:
```
gcloud projects add-iam-policy-binding ${PROJECT_ID} \
  --member user:"${GCP_EMAIL_ADDRESS}" \
  --role=roles/editor \
  --role=roles/compute.admin \
  --role=roles/container.admin \
  --role=roles/resourcemanager.projectIamAdmin \
  --role=roles/iam.serviceAccountAdmin \
  --role=roles/iam.serviceAccountKeyAdmin \
  --role=roles/gkehub.admin

gcloud services enable \
  container.googleapis.com \
  compute.googleapis.com \
  monitoring.googleapis.com \
  logging.googleapis.com \
  cloudtrace.googleapis.com \
  meshca.googleapis.com \
  meshtelemetry.googleapis.com \
  meshconfig.googleapis.com \
  iamcredentials.googleapis.com \
  anthos.googleapis.com \
  gkeconnect.googleapis.com \
  gkehub.googleapis.com \
  cloudresourcemanager.googleapis.com

gcloud config set compute/region ${CLUSTER_LOCATION}

gcloud container clusters get-credentials ${CLUSTER_NAME}

kubectl create clusterrolebinding cluster-admin-binding \
  --clusterrole=cluster-admin \
  --user="$(gcloud config get-value core/account)"

gcloud iam service-accounts create ${SERVICE_ACCOUNT_NAME}

gcloud projects add-iam-policy-binding ${PROJECT_ID} \
  --member="serviceAccount:${SERVICE_ACCOUNT_NAME}@${PROJECT_ID}.iam.gserviceaccount.com" \
  --role="roles/gkehub.connect"

gcloud iam service-accounts keys create ${SERVICE_ACCOUNT_KEY_PATH} \
  --iam-account=${SERVICE_ACCOUNT_NAME}@${PROJECT_ID}.iam.gserviceaccount.com

gcloud container hub memberships register "${MEMBERSHIP_NAME}" \
  --gke-cluster=${CLUSTER_LOCATION}/${CLUSTER_NAME} \
  --service-account-key-file=${SERVICE_ACCOUNT_KEY_PATH}

istioctl manifest apply --set profile=asm \
  --set values.global.trustDomain=${WORKLOAD_POOL} \
  --set values.global.sds.token.aud=${WORKLOAD_POOL} \
  --set values.nodeagent.env.GKE_CLUSTER_URL=https://container.googleapis.com/v1/projects/${PROJECT_ID}/locations/${CLUSTER_LOCATION}/clusters/${CLUSTER_NAME} \
  --set values.global.meshID=${MESH_ID} \
  --set values.global.proxy.env.GCP_METADATA="${PROJECT_ID}|${PROJECT_NUMBER}|${CLUSTER_NAME}|${CLUSTER_LOCATION}"
```

## Step 5: Deploy Kubeflow

1. Set environment variables:
```
export KF_NAME=${CLUSTER_NAME}
export KF_PROJECT=${MGMT_PROJECT}
export KF_DIR=<path to your Kubeflow configuration directory>
export MGMTCTXT=<your kubectl context>
export CLIENT_ID=<Your CLIENT_ID>         # You created this in Step 2
export CLIENT_SECRET=<Your CLIENT_SECRET> # You created this in Step 2
```
2. Log in:
```
gcloud auth login
```
3. Fetch the Kubeflow package, change to the Kubeflow directory, and fetch Kubeflow manifests:
```
kpt pkg get https://github.com/kubeflow/gcp-blueprints.git/kubeflow@v1.2.0 "${KF_DIR}"

cd "${KF_DIR}"

make get-pkg
```
4. **Edit ${KF_DIR}/Makefile to set the parameters to the desired values** (lines 57-71).
5. Choose the management cluster context, create a namespace in your management cluster, make the Kubeflow project’s namespace default of the `${MGMTCTXT}` context, invoke the make rule to set the kpt setters, and deploy Kubeflow:
```
kubectl config use-context "${MGMTCTXT}"

kubectl create namespace "${KF_PROJECT}"

kubectl config set-context --current --namespace "${KF_PROJECT}"

make set-values

make apply
```

## Notes

**Pros:**
- Kubeflow utilizes the composability, portability, and scalability of Kubernetes
- Kubeflow includes powerful tools like Anthos Service Mesh, Seldon Core, KFServing, etc

**Cons:**
- The GCP Kubeflow YouTube [videos](https://www.youtube.com/playlist?list=PLIivdWyY5sqLS4lN75RPDEyBgTro_YX7x) are outdated; Kubeflow deployment is no longer supported through the GCP dashboard
- Overall docs and support are so out of sync; GCP dashboard is weird too. I'm going back to AWS 😂